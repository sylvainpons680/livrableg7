<?php
setcookie('user_id', '1234');
setcookie('user_pref', 'dark_theme', time() + 3600 * 24, '/', '', true, true);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Document</title>
</head>

<body>
<header>
            <nav class="nav navbar-dark bg-dark">
                
                <a class=" navbar-brand nav-link" href="/index.html">Page d'accueil</a>
               
              </nav>
        </header>
        <div id="mydiv">Il faut rafraichir la page pour voir le cookie</div>
        <?php
if (isset($_COOKIE['user_id'])) {
    echo 'Votre ID de session est le ' . $_COOKIE['user_id'];
}
?>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example15-modal-lg">Explication</button>
    
    <div class="modal fade bd-example15-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <p>Je crée un cookie avec la methode setcookie et je lui donne l'id que je souhaite</p>
            
        </div>
      </div>
    </div>
        <div class="card container" style="width: 22rem;">
  <img class="card-img-top" src="/img/Red.jpg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Image de la métadonnée</h5>
  </div>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example2-modal-lg">Explication</button>
      
      <div class="modal fade bd-example2-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <p>J'ouvre l'image avec la methode fopen ensuite avec exif_read_data je vais lire les Metadonnée de l'entente</p>
              
          </div>
        </div>
      </div>
</div>
    
    <?php
    // Ouvrir le fichier, cela devrait être en mode binaire
    $fp = fopen('./img/Red.jpg', 'rb');

    if (!$fp) {
        echo 'Error: Unable to open image for reading';
        exit;
    }

    // Essayez de lire les en-têtes EXIF
    $headers = exif_read_data($fp);

    if (!$headers) {
        echo 'Error: Unable to read exif headers';
        exit;
    }

    // Afficher les entêtes 'COMPUTED'
    echo '<div class="container">Metadonnée d\une image:</div>' . PHP_EOL;

    foreach ($headers['COMPUTED'] as $header => $value) {
        printf('<div class="container"><div> %s => %s%s</div></div>', $header, $value, PHP_EOL);
    }
    ?>
   
  
     <div class="container">
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example2-modal-lg">Explication</button>
    
    <div class="modal fade bd-example2-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <p>Pour intégrer une iframe je suis allez sur google map et j'ai récuperer le code html que google m'as donner</p>
            
        </div>
      </div>
    </div>
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d92457.01034588464!2d1.3628015591588944!3d43.60067857114025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebb6fec7552ff%3A0x406f69c2f411030!2sToulouse!5e0!3m2!1sfr!2sfr!4v1583797112797!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="app.js"></script>

</body>

</html>